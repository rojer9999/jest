import CartParser from './CartParser';

let parser, parseLine, validate, csvStringData, jsonStringData, itemsFromJson;

const correctUuid = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;

beforeEach(() => {
	parser = new CartParser();
	validate = parser.validate.bind(parser)
	parseLine = parser.parseLine.bind(parser);
	csvStringData = parser.readFile('./samples/cart.csv');
	jsonStringData = parser.readFile('./samples/cart.json');
	itemsFromJson = JSON.parse(jsonStringData).items;
});

describe('CartParser - unit tests', () => {

	describe('validate', () => {
		it('Should return []', () => {
			expect(validate(csvStringData)).toEqual([]);
		});

		it('Should return [err] if there is a wrong header', () => {
			const header = 'Product name,Price,Total'; // the Total header instead of Quantity
			const err = {
				type: 'header',
				row: 0,
				column: 2,
				message: 'Expected header to be named "Quantity" but received Total.'
			};

			expect(validate(header)).toEqual([err]);
		});

		it('Should return [err] if a cell is absent in a row', () => {
			const line = `Product name,Price,Quantity\n
										Mollis consequat,9.00`; // absent the last cell
			const err = {
				type: 'row',
				row: 1,
				column: -1,
				message: 'Expected row to have 3 cells but received 2.'
			};

			expect(validate(line)).toEqual([err]);
		});

		it('Should return [err] if there is an empty value in a cell', () => {
			const line = `Product name,Price,Quantity\n
										,9.00,2`; // an empty cell instead of String 'Mollis consequat'
			const err = {
				type: 'cell',
				row: 1,
				column: 0,
				message: 'Expected cell to be a nonempty string but received "".'
			};

			expect(validate(line)).toEqual([err]);
		});

		it('Should return [err] if there is a wrong csvStringData type in a line', () => {
			const line = `Product name,Price,Quantity\n
										Mollis consequat,9.00,\'2\'`; // a String '2' instead of Number 2
			const err = {
				type: 'cell',
				row: 1,
				column: 2,
				message: 'Expected cell to be a positive number but received "\'2\'".'
			};

			expect(validate(line)).toEqual([err]);
		});

		it('Should return [err] if there is a negative number in a cell', () => {
			const line = `Product name,Price,Quantity\n
										Mollis consequat,9.00,-2`; // a negative value -2 instead of 2
			const err = {
				type: 'cell',
				row: 1,
				column: 2,
				message: 'Expected cell to be a positive number but received "-2".'
			};

			expect(validate(line)).toEqual([err]);
		});
	});

	describe('parseLine', () => {
		it('Should return an object', () => {
			const objFromCsvLine = {
				name: 'Mollis consequat',
				price: 9,
				quantity: 2,
				id: expect.stringMatching(correctUuid)
			}
			
			expect(parseLine('Mollis consequat,9.00,2')).toMatchObject(objFromCsvLine);
		});
	});

	describe('output json', () => {
		it('Should return an error if the id is wrong', () => {
			for (const iterator of itemsFromJson) {
				expect(iterator.id).toMatch(correctUuid);
			}
		});
		
		it('Should return an error if there is no some field', () => {
			for (const iterator of itemsFromJson) {
				const { id, name, price, quantity } = iterator;
				expect(id && name && price && quantity).toBeTruthy();
			}
		});
	});
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
});